package clazz;

import org.junit.Test;

public class UserTest {
    @Test
    public void classTest() throws Exception {
        System.out.println("根据类名：\t\t" + User.class);
        System.out.println("根据对象：\t\t" + new User().getClass());
        System.out.println("根据全限定类名：\t" + Class.forName("clazz.User"));

        Class userClass = User.class;
        System.out.println("getName(): " + userClass.getName());
        System.out.println("getSimpleName(): " + userClass.getSimpleName());
        System.out.println("newInstance(): " + userClass.newInstance());
    }
}
