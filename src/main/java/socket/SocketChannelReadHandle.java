package socket;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.nio.charset.StandardCharsets;

@Slf4j
class SocketChannelReadHandle implements CompletionHandler<Integer, StringBuffer> {

    private final AsynchronousSocketChannel socketChannel;
    private final ByteBuffer byteBuffer;

    public SocketChannelReadHandle(AsynchronousSocketChannel socketChannel, ByteBuffer byteBuffer) {
        this.socketChannel = socketChannel;
        this.byteBuffer = byteBuffer;
    }


    @Override
    public void completed(Integer result, StringBuffer historyContext) {
        if (result == -1) {
            try {
                this.socketChannel.close();
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
            return;
        }

        log.info("completed(Integer result, Void attachment) : 然后我们来取出通道中准备好的值");
        this.byteBuffer.flip();
        byte[] contexts = new byte[1024];
        this.byteBuffer.get(contexts, 0, result);
        this.byteBuffer.clear();
        String nowContent = new String(contexts, 0, result, StandardCharsets.UTF_8);
        historyContext.append(nowContent);
        log.info("================目前的传输结果: " + historyContext);

        if (historyContext.indexOf("over") == -1) {
            return;
        }

        log.info("=======收到完整信息，开始处理业务=========");
        historyContext = new StringBuffer();

        this.socketChannel.read(this.byteBuffer, historyContext, this);
    }

    @Override
    public void failed(Throwable exc, StringBuffer attachment) {
        log.info("=====发现客户端异常关闭，服务器将关闭TCP通道");
        try {
            this.socketChannel.close();
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
    }
}
