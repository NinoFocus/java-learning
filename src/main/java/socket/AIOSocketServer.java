package socket;

import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.BasicConfigurator;

import java.net.InetSocketAddress;
import java.nio.channels.AsynchronousChannelGroup;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
public class AIOSocketServer {
    private static final Object waitObject = new Object();

    static {
        BasicConfigurator.configure();
    }

    public static void main(String[] args) throws Exception {
        ExecutorService threadPool = Executors.newFixedThreadPool(20);
        AsynchronousChannelGroup group = AsynchronousChannelGroup.withThreadPool(threadPool);
        final AsynchronousServerSocketChannel serverSocket = AsynchronousServerSocketChannel.open(group);

        serverSocket.bind(new InetSocketAddress("0.0.0.0", 83));
        serverSocket.accept(null, new ServerSocketChannelHandle(serverSocket));

        synchronized (waitObject) {
            waitObject.wait();
        }
    }

}
