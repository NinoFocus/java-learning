package concurrent.thread;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

public class MyCallable implements Callable<Integer> {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        MyCallable myCallable = new MyCallable();
        Future<Integer> ft = new FutureTask<>(myCallable);
        Thread thread = new Thread((Runnable) ft);
        thread.start();
        System.out.println(ft.get());
    }

    @Override
    public Integer call() throws Exception {
        return 123;
    }
}
