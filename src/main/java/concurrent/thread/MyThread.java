package concurrent.thread;

public class MyThread extends Thread {
    public static void main(String[] args) {
        MyThread mt = new MyThread();
        mt.start();
    }

    public void run() {
        System.out.println("MyThread run()");
    }
}
