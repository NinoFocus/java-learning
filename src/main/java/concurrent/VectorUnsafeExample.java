package concurrent;

import java.util.Vector;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class VectorUnsafeExample {
    private static final Vector<Integer> vector = new Vector<>();

    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            vector.add(i);
        }
        ExecutorService executorService = Executors.newCachedThreadPool();

//      this code will cause ArrayIndexOutOfBoundsException
//        executorService.execute(() -> {
//            for (int i = 0; i < vector.size(); i++) {
//                System.out.println("remove index: " + i);
//                vector.remove(i);
//            }
//        });
//
//        executorService.execute(() -> {
//            for (int i = 0; i < vector.size(); i++) {
//                System.out.println("get index: " + i);
//                vector.remove(i);
//                vector.get(i);
//            }
//        });

        executorService.execute(() -> {
            synchronized (vector) {
                for (int i = 0; i < vector.size(); i++) {
                    System.out.println("remove index: " + i);
                    vector.remove(i);
                }
            }
        });

        executorService.execute(() -> {
            synchronized (vector) {
                for (int i = 0; i < vector.size(); i++) {
                    System.out.println("get index: " + i);
                    vector.remove(i);
                    vector.get(i);
                }
            }
        });

        System.out.println(vector);

        executorService.shutdown();
    }
}
