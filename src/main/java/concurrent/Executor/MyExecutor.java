package concurrent.Executor;

import concurrent.thread.MyRunnable;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MyExecutor {
    public static void main(String[] args) {
        /**
         * - CachedThreadPool: 一个任务创建一个线程
         * - FixedThreadPool: 所有任务只能使用固定大小的线程
         * - SingleThreadExecutor: 相当于大小为 1 的FixedThreadPool
         */
        ExecutorService executorService = Executors.newCachedThreadPool();
        for (int i = 0; i < 5; i++) {
            executorService.execute(new MyRunnable());
        }
        executorService.shutdown();
    }
}
